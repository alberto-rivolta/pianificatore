import java.util.ArrayList;
import java.util.Collections;


public class Ordine implements Comparable<Ordine> {

	public int id;
	public int qta;
	public int rilascio;
	public Nodo origine;
	public Nodo destinazione;
	public int duedate;
	public int deadline;
	public ArrayList<Frammento> frammenti;
	public boolean attivo = true;
	public int arrivato = 0;
	public boolean scaduto = false;

	public Ordine(int i, int q, int r, int id_o, int id_d, int dd, int dl) {
		id = i;
		qta = q;
		rilascio = r;
		origine = Rete.nodo(id_o);
		destinazione = Rete.nodo(id_d);
		duedate = dd;
		deadline = dl;
		frammenti = new ArrayList<Frammento>();
	}
	public void schedula_partenze(Nodo nodo) {
		Frammento frammento = frammento_non_schedulato(nodo);
		if (nodo.tipo == Punto.consolidamento) {
			if (nodo.area == origine.area) {
				ArrayList<Percorso> percorsi = nodo.percorsi_verso(destinazione.area);
				boolean completato = false;
				while (!completato) {
					for (Percorso percorso : percorsi) {
						if (percorso.capacita - percorso.traffico[Orologio.giorno] < 1)
							continue;
						else if (frammento.qta > percorso.capacita - percorso.traffico[Orologio.giorno]) {
							Frammento f = new Frammento(this, (int)percorso.capacita - percorso.traffico[Orologio.giorno], nodo, percorso);
							frammenti.add(f);
							frammento.qta -= f.qta;
							percorso.traffico[Orologio.giorno] += f.qta;
							System.out.println(f.qta + "u di " + this + " andranno " + percorso);
							risorse_frammentazione_partenze(f, percorso.tipo == Tratto.lungo ? percorso.archi.get(0) : Rete.arco(nodo, percorso.partenza), nodo);
						} else {
							frammento.percorso = percorso;
							percorso.traffico[Orologio.giorno] += frammento.qta;
							System.out.println(frammento.qta + "u di " + this + " andranno " + percorso);
							risorse_frammentazione_partenze(frammento, percorso.tipo == Tratto.lungo ? percorso.archi.get(0) : Rete.arco(nodo, percorso.partenza), nodo);
							completato = true;
							break;
						}
					}
				}
			} else {
				System.out.println("schedulata partenza per ultimo miglio");
				Rete.eventi.add(new Partenza(frammento, Orologio.tempo + Orologio.epsilon, Rete.arco(nodo, destinazione), nodo, Rete.risorsa(Rete.id_risorsa_locale)));
			}
		} else {
			Arco arco = null;
			if (nodo.area == origine.area) {
				arco = frammento.percorso.archi.get(0);
			} else if (nodo.area == destinazione.area) {
				arco = Rete.arco(Rete.consolidamento(destinazione.area), frammento.percorso.arrivo);
			} else {
				for (int a = 0; a < frammento.percorso.archi.size() - 1; a++) {
					if(frammento.percorso.archi.get(a).n == nodo || frammento.percorso.archi.get(a).m == nodo) {
						arco = frammento.percorso.archi.get(a + 1);
						break;
					}
				}
			}
			System.out.println("prossimo arco: " + arco);
			risorse_frammentazione_partenze(frammento, arco, nodo);
		}
		System.out.println("frammenti " + this + ": ");
		for (Frammento f : frammenti)
			System.out.println("\t" + f);
	}
	public void risorse_frammentazione_partenze(Frammento frammento, Arco arco, Nodo nodo) {
		double tempo_restante = frammento.percorso.tempo_restante(nodo);
		System.out.println("tempo restante: " + tempo_restante);
		double partenza_max_duedate = frammento.ordine.duedate + 1 - Orologio.epsilon - tempo_restante;
		double partenza_max_deadline = frammento.ordine.deadline + 1 - Orologio.epsilon - tempo_restante;
		System.out.println("partenza massima per duedate: " + partenza_max_duedate);
		System.out.println("partenza massima per deadline: " + partenza_max_deadline);
		ArrayList<Risorsa> risorse = arco.risorse();
		boolean completato = false;
		while (!completato) {
			Graduatoria graduatoria = new Graduatoria(nodo, arco, risorse);
			System.out.println("graduatoria mezzi " + arco + " da " + nodo + ": ");
			for (Grado grado : graduatoria.voci)
				System.out.println("\t" + grado);
			for (Grado grado : graduatoria.voci) {
				if (grado.utile != null) {
					if (grado.utile.tempo > partenza_max_duedate) {
						System.out.println("\t\t" + "partenza oltre tempo utile per duedate...");
						if (grado.utile.tempo < partenza_max_deadline) {
							System.out.println("\t\t" + "... ma entro tempo utile per deadline");
							if (Rete.generatore.nextDouble() > Rete.probabilita_raggruppamento_vs_duedate) {
								System.out.println("\t\t" + "scelto rispetto duedate (se possibile)");
								if (Collections.min(graduatoria.voci).tempo < grado.utile.tempo) {
									continue;
								}
							}
							System.out.println("\t\t" + "scelto rispetto raggruppamento");
						} else {
							System.out.println("\t\t" + "... e oltre tempo utile per deadline");
							if (Collections.min(graduatoria.voci).tempo < grado.utile.tempo) {
								System.out.println("\t\t" + "ci sono candidati migliori");
								continue;
							}
							System.out.println("\t\t" + "non ci sono candidati migliori");
						}
					}
					if (grado.risorsa.capacita_max() - grado.utile.carico() >= frammento.qta) {
						System.out.println(frammento + " aggiunti a " + grado.utile);
						grado.utile.frammenti.add(frammento);
						deframmenta(grado.utile, frammento);
						completato = true;
						break;
					} else {
						Frammento f = new Frammento(this, grado.risorsa.capacita_max() - grado.utile.carico(), nodo, frammento.percorso);
						System.out.println(f + " aggiunti a " + grado.utile);
						grado.utile.frammenti.add(f);
						frammenti.add(f);
						frammento.qta -= f.qta;
						deframmenta(grado.utile, f);
					}
				} else {
					Partenza partenza = new Partenza(grado.tempo, arco, nodo, grado.risorsa);
					if (frammento.qta <= grado.risorsa.capacita_max()) {
						partenza.frammenti.add(frammento);
						completato = true;
					} else {
						Frammento f = new Frammento(this, grado.risorsa.capacita_max(), nodo, frammento.percorso);
						partenza.frammenti.add(f);
						frammenti.add(f);
						frammento.qta -= f.qta;
					}
					Rete.eventi.add(partenza);
					for (Partenza p : grado.ulteriori) {
						Rete.eventi.add(p);
						System.out.println("aggiunta partenza vuota: " + p);
					}
					System.out.println("aggiunta " + partenza);
					if (completato)
						break;
				}
			}
		}
	}
	public Frammento frammento_non_schedulato(Nodo n) {
		for (Frammento frammento : frammenti)
			if (frammento.nodo == n && !frammento.schedulato())
				return frammento;
		return null;
	}
	public void deframmenta(Partenza partenza, Frammento frammento) {
		Frammento parente = null;
		for (Frammento f : partenza.frammenti) {
			if (f != frammento && f.ordine == frammento.ordine) {
				parente = f;
				break;
			}
		}
		if (parente != null) {
			parente.qta += frammento.qta;
			partenza.frammenti.remove(frammento);
			frammento.ordine.frammenti.remove(frammento);
		}
	}
	public void deframmenta(Nodo nodo) {
		Frammento frammento_dreframmentato = new Frammento(this, 0, nodo);
		ArrayList<Frammento> frammenti_da_deframmentare = new ArrayList<>();
		for (Frammento frammento : frammenti) {
			if (frammento.nodo == nodo && !frammento.schedulato()) {
				frammento_dreframmentato.qta += frammento.qta;
				frammenti_da_deframmentare.add(frammento);
			}
		}
		for (Frammento frammento : frammenti_da_deframmentare) {
			frammenti.remove(frammento);
			frammento_dreframmentato.percorso = frammento.percorso;
		}
		frammenti.add(frammento_dreframmentato);
	}
	public boolean arrivato() {
		return arrivato == qta;
	}
	public int compareTo(Ordine o) {
		return -Integer.compare(duedate, o.duedate);
	}
	public String toString() {
		return "ordine " + id;
	}
}
