import java.sql.SQLException;
import java.util.Random;

public class Main {
	
	public static void main(String[] args) {
		
		double inizio = System.currentTimeMillis();
			
		try {
			Base.percorso_base = args[0];
			Base.istanza = Integer.parseInt(args[1]);
			Base.percorso_uscita_piano = args[2];
			Base.percorso_uscita_euristica = args[3];
		} catch (Exception e) {
			System.err.println("errore durante la lettura dei parametri ingresso/uscita");
			return;
		}
					
		Rete.ritardo_massimo = 0.0;
		Rete.penalita_ordine_non_consegnato = 1000000.0;
		Rete.penalita_carico_minimo_treno = 0;
		Rete.probabilita_raggruppamento_vs_duedate = 0.8;
		Rete.sovrastima = 0.10;
		
		try {
			long seme = Long.parseLong(args[4]);
			Rete.generatore = seme > 0 ? new Random(seme) : new Random();
			Rete.crea();
		} catch (SQLException e) {
			System.err.println("errore durante la creazione della rete");
			return;
		}
		
		/*
		// FIXME
		Rete.costo_trasporto_vuoto = 2;
		Rete.capacita_max_treno = 2500;
		Rete.capacita_min_treno = 500;
		Rete.penalita_carico_minimo_treno = 1000000;
		*/
		
		double lettura = System.currentTimeMillis();
						
		Tratto.ferrovia.priorita = 1;
		Tratto.medio.priorita = 2;
		Tratto.lungo.priorita = 3;
		
		Orologio.soglia_mattino_pomeriggio = 0.5;
		Orologio.ore_lavorative = 8;
				
		Euristica.peggioramento_iniziale_accettato = 0.1;
		Euristica.ricettivita_peggioramento_iniziale = 0.9;
		Euristica.coefficiente_raffreddamento = 0.9;
		Euristica.temperatura_congelamento = 0.1;
				
		Euristica euristica;
		try {
			Euristica.iterazioni_per_raffreddamento = Integer.parseInt(args[5]);
			
			double ampiezza = Double.parseDouble(args[6]);
			Euristica.mosse.add(new Scambio());
			Euristica.mosse.add(new Scambio(ampiezza));
			Euristica.mosse.add(new Inserimento());
			Euristica.mosse.add(new Inserimento(ampiezza));
			
			switch (Integer.parseInt(args[7])) {
				case Euristica.CASUALE:
					euristica = new Casuale();
					break;
				case Euristica.SINGOLA:
					euristica = new Singola(Euristica.mosse.get(Integer.parseInt(args[8])));
					break;
				case Euristica.VND:
					euristica = new VND();
					break;
				case Euristica.RISCALDAMENTO:
					euristica = new Riscaldamento(Integer.parseInt(args[8]), Integer.parseInt(args[9]));
					break;
				default:
					return;
			}
		} catch (Exception e) {
			System.err.println("errore durante le lettura dei parametri euristica");
			return;
		}
		System.out.println("euristica: " + euristica);
		euristica.esegui();
		
		double fine = System.currentTimeMillis();
		
		System.out.println(inizio + "\t" + lettura + "\t" + fine);
	}
}
