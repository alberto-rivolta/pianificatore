

public class Treno extends Risorsa {

	public int frequenza;
	public Orario orario;
	public boolean materiale_in_n;
	public boolean materiale_in_m;
	public int violazioni_capacita_minima = 0;

	public Treno(int id_r, Arco a, int f, int o_ap) {
		id = id_r;
		arco = a;
		frequenza = f;
		orario = o_ap == 1 ? Orario.pomeriggio : Orario.mattina;
		materiale_in_n = true;
		materiale_in_m = true;
	}
	public int capacita_max(){
		return Rete.capacita_max_treno;
	}
	public int capacita_min() {
		return Rete.capacita_min_treno;
	}
	public double velocita() {
		return Rete.coefficiente_treno_camion * Rete.velocita_camion;
	}
	public void carica(Partenza partenza) {
		super.carica(partenza);
		if (partenza.nodo == arco.n) {
			materiale_in_n = false;
			System.out.println(this + " ha lasciato " + arco.n);
		} else {
			materiale_in_m = false;
			System.out.println(this + " ha lasciato " + arco.m);
		}
		if (partenza.carico() < capacita_min())
			violazioni_capacita_minima++;
	}
	public void scarica(Arrivo arrivo) {
		super.scarica(arrivo);
		System.out.println(this + " è arrivato in " + arrivo.nodo);
		utilizzata++;
	}
	public boolean in_partenza() {
		return Orologio.giorno % frequenza == 0 && Orologio.orario == orario;
	}
	public boolean disponibile(Partenza partenza) {
		return in_partenza() && (partenza.nodo == arco.n ? materiale_in_n == true : materiale_in_m == true) && partenza.carico() <= capacita_max();
	}
	public double prossima_partenza() {
		int giorni = Orologio.giorno;
		double ore = Orologio.tempo - giorni;
		if (ore > Orologio.soglia_mattino_pomeriggio || (ore > 0 && orario == Orario.mattina)) 
			giorni++;
		return Orologio.giorno_orario_in_tempo((int)(((double)giorni / (double)frequenza) * frequenza) + (giorni % frequenza), orario);
	}
	public String toString() {
		return "treno " + id;
	}
}
