import java.util.ArrayList;
import java.util.Collections;


public abstract class Risorsa {

	public int id;
	public double costi_trasporto = 0;
	public int utilizzata = 0;
	public Arco arco;

	public abstract int capacita_max();
	public abstract double velocita();
	public abstract boolean disponibile(Partenza partenza);
	public void scarica(Arrivo arrivo) {
		for (Frammento frammento : arrivo.frammenti) {
			frammento.nodo = arrivo.nodo;
			System.out.println("scaricate " + frammento + " in " + arrivo.nodo + " da " + arrivo.risorsa + " partita da " + arrivo.arco.altro_capo(frammento.nodo));
		}
		switch (arrivo.arco.tipo) {
			case locale:
				costi_trasporto += Rete.costo_breve_distanza * arrivo.arco.lunghezza * arrivo.carico();
				break;
			case medio:
				costi_trasporto += Rete.costo_media_distanza * arrivo.arco.lunghezza * arrivo.carico();
				break;
			case lungo:
				costi_trasporto += Rete.costo_lunga_distanza * arrivo.arco.lunghezza * arrivo.carico();
				break;
			case ferrovia:
				costi_trasporto += Rete.costo_ferrovia * arrivo.arco.lunghezza * arrivo.carico();
				break;
		}
	}
	public void carica(Partenza partenza) {
		for (Frammento frammento : partenza.frammenti) {
			System.out.println("caricate " + frammento + " in " + partenza.nodo + " su " + this + " verso " + partenza.arco.altro_capo(partenza.nodo));
			partenza.nodo.magazzino -= frammento.qta;
			frammento.nodo = null;
		}
	}
	public ArrayList<Evento> eventi() {
		ArrayList<Evento> eventi = new ArrayList<>();
		for (Evento evento : Rete.eventi)
			if (evento.attivo && evento.risorsa == this)
				eventi.add(evento);
		Collections.sort(eventi);
		return eventi;
	}
}
