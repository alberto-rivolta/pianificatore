import java.util.ArrayList;


public class Percorso implements Comparable<Percorso> {

	public Nodo partenza;
	public Nodo arrivo;
	public ArrayList<Arco> archi = new ArrayList<>();
	public double capacita;
	public Tratto tipo = Tratto.medio;
	public int[] traffico;
	
	public Percorso(Nodo p, Nodo a, Nodo[] precedente) {
		arrivo = a;
		partenza = p;
		traffico = new int[Orologio.orizzonte];
		Nodo nodo = a;
		while (precedente[nodo.id] != null) {
			Arco arco = Rete.arco(nodo, precedente[nodo.id]);
			archi.add(0, arco);
			nodo = precedente[nodo.id];
		}
		if (archi.size() > 1) {
			partenza = archi.get(0).altro_capo(partenza);
			arrivo = archi.get(archi.size() - 1).altro_capo(arrivo);
			archi.remove(0);
			archi.remove(archi.size() - 1);
		}
		double risorse = 0;
		for (Arco arco : archi) {
			if (arco.tipo == Tratto.ferrovia) {
				tipo = Tratto.ferrovia;
				risorse = 0;
				for (Risorsa risorsa : Rete.risorse)
					if (risorsa instanceof Treno && ((Treno)risorsa).arco == arco)
						risorse += (double)1 / ((Treno)risorsa).frequenza;
				capacita = risorse * Rete.capacita_max_treno;
				return;
			}
			if (arco.tipo == Tratto.lungo)
				tipo = Tratto.lungo;
			int t = 0;
			for (Risorsa risorsa : Rete.risorse)
				if (risorsa instanceof Camion && ((Camion)risorsa).arco == arco)
					t++;
			if (t < risorse || risorse == 0)
				risorse = t;
		}
		capacita = risorse * Rete.capacita_max_camion;
	}
	public double tempo_restante(Nodo n) {
		double tempo = (Rete.lunghezza_max_locale / Rete.velocita_locale) / Orologio.ore_lavorative + (Rete.lunghezza_max_consolidamento / Rete.velocita_camion) / Orologio.ore_lavorative;
		if (n.area == partenza.area && n.tipo == Punto.consolidamento && tipo != Tratto.lungo)
			tempo += (Rete.lunghezza_max_consolidamento / Rete.velocita_camion) / Orologio.ore_lavorative;
		for (int a = archi.size() - 1; a >= 0; a--) {
			tempo += (archi.get(a).lunghezza / (tipo == Tratto.ferrovia ? Rete.velocita_camion * Rete.coefficiente_treno_camion : Rete.velocita_camion)) / Orologio.ore_lavorative;
			if(archi.get(a).n == n || archi.get(a).m == n)
				break;
		}
		return tempo * (1 + Rete.sovrastima);
	}
	public int compareTo(Percorso p) {
		return Integer.compare(tipo.priorita, p.tipo.priorita);
	}
	public String toString() {
		return "da " + partenza + " a " + arrivo + " (" + tipo + ", " + capacita + "u)";
	}
}
