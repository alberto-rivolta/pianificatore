
public class Frammento {

	public Ordine ordine;
	public int qta;
	public Nodo nodo;
	public Percorso percorso;

	public Frammento(Ordine o, int q, Nodo n) {
		ordine = o;
		qta = q;
		nodo = n;
	}
	public Frammento(Ordine o, int q, Nodo n, Percorso p) {
		this(o, q, n);
		percorso = p;
	}
	public boolean schedulato() {
		for (Evento evento : Rete.eventi)
			if (evento instanceof Partenza && evento.attivo && evento.frammenti.contains(this))
				return true;
		return false;
	}
	public String toString() {
		return qta + "u di " + ordine;
	}
}
