import java.util.ArrayList;


public class VND extends Euristica {

	private ArrayList<Mossa> usate = new ArrayList<>();
	
	public Mossa prossima_mossa() {
		if (miglioramento > 0)
			return mossa;
		else {
			if (mosse.size() == 0) {
				for (Mossa m : usate)
					mosse.add(m);
				usate.clear();
			}
			Mossa m = mosse.remove(Rete.generatore.nextInt(mosse.size()));
			usate.add(m);
			return m;
		}
	}
	public String toString() {
		return "simulated annealing con variable neighborhood descendant";
	}
}
