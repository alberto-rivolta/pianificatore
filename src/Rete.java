import java.io.FileWriter;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;


public class Rete {
	
	public static int capacita_max_camion;
	public static int capacita_max_treno;
	public static int capacita_min_treno;
	public static int capacita_max_locale;
	public static int numero_aree;
	public static int velocita_camion;
	public static int velocita_locale;
	public static double coefficiente_treno_camion;
	public static double costo_lunga_distanza;
	public static double costo_media_distanza;
	public static double costo_breve_distanza;
	public static double costo_ferrovia;
	public static double costo_magazzino_origine;
	public static double costo_magazzino_consolidamento;
	public static double costo_magazzino_ferroviario;
	public static double costo_magazzino_gomma;
	public static double costo_ritardo;
	public static double costo_fisso_media_distanza;
	public static double costo_fisso_lunga_distanza;
	public static double costo_fisso_treno;
	public static double costo_trasporto_vuoto;
	public static double ritardo_massimo;
	public static double penalita_ordine_non_consegnato;
	public static double penalita_carico_minimo_treno;
	public static ArrayList<Nodo> nodi = new ArrayList<>();
	public static ArrayList<Arco> archi = new ArrayList<>();
	public static ArrayList<Ordine> ordini = new ArrayList<>();
	public static ArrayList<Risorsa> risorse = new ArrayList<>();
	public static ArrayList<Evento> eventi = new ArrayList<>();
	public static ArrayList<Percorso> percorsi = new ArrayList<>();
	public static final int id_risorsa_locale = -1;
	public static double lunghezza_max_locale = 0;
	public static double lunghezza_max_consolidamento = 0;
	public static double probabilita_raggruppamento_vs_duedate;
	public static double sovrastima;
	public static Random generatore;
	
	public static void crea() throws SQLException {
		Base.leggi();
		
		risorse.add(new Locale(id_risorsa_locale));

		for (Arco arco : archi) {
			if (arco.tipo == Tratto.locale && lunghezza_max_locale < arco.lunghezza)
				lunghezza_max_locale = arco.lunghezza;
			if (arco.tipo == Tratto.medio && lunghezza_max_consolidamento < arco.lunghezza)
				lunghezza_max_consolidamento = arco.lunghezza;
		}
		
		for (Nodo nodo : nodi)
			if (nodo.tipo == Punto.consolidamento) {
				boolean[] visitato = new boolean[Collections.max(nodi).id + 1];
				Nodo[] precedente = new Nodo[Collections.max(nodi).id + 1];
				ArrayList<Nodo> coda = new ArrayList<>();
				coda.add(nodo);
				while (coda.size() > 0) {
					Nodo n = coda.remove(0);
					visitato[n.id] = true;
					for (Nodo m : n.adiacenti) {
						if (!visitato[m.id] && m.tipo != Punto.origine) {
							if (m.tipo == Punto.consolidamento && m.area != nodo.area) {
								precedente[m.id] = n;
								nodo.percorsi.add(aggiungi_percorso(new Percorso(nodo, m, precedente)));
							} else {
								if(!coda.contains(m)) {
									precedente[m.id] = n;
									coda.add(m);
								}
							}
						}
					}
				}
			}
	}
	
	public static Nodo nodo(int id) {
		for (Nodo nodo: nodi)
			if (nodo.id == id)
				return nodo;
		return null;
	}
	public static Arco arco(Nodo n, Nodo m) {
		for (Arco arco : archi)
			if ((arco.n == n && arco.m == m) || (arco.n == m && arco.m == n))
				return arco;
		return null;
	}
	public static Ordine ordine(int id_ordine) {
		for (Ordine ordine : ordini)
			if (ordine.id == id_ordine)
				return ordine;
		return null;
	}
	public static Risorsa risorsa(int id_risorsa) {
		for (Risorsa risorsa : risorse)
			if (risorsa.id == id_risorsa)
				return risorsa;
		return null;
	}
	public static Evento prossimo_evento() {
		Collections.sort(eventi);
		for (Evento evento : eventi)
			if (evento.imminente())
				return evento;
		return null;
	}
	public static int carico_locale(Nodo consolidamento) {
		int carico = 0;
		for (Evento evento : eventi)
			if (evento instanceof Partenza && (int)evento.tempo == Orologio.giorno && evento.arco.tipo == Tratto.locale && (evento.arco.n == consolidamento || evento.arco.m == consolidamento))
				carico += evento.carico();
		return carico;
	}
	public static void rilasci() {
		for (Ordine ordine : ordini)
			if (Orologio.giorno == ordine.rilascio && Orologio.orario == Orario.mattina) {
				ordine.origine.magazzino += ordine.qta;
				if (!ordine.attivo)
					continue;
				Frammento frammento = new Frammento(ordine, ordine.qta, ordine.origine);
				ordine.frammenti.add(frammento);
				Nodo consolidamento = consolidamento(ordine.origine.area);
				eventi.add(new Partenza(frammento, Orologio.tempo, arco(ordine.origine, consolidamento), ordine.origine, risorsa(id_risorsa_locale)));
			}
	}
	public static Nodo consolidamento(int a) {
		Nodo consolidamento = null;
		for (Nodo nodo : nodi)
			if (nodo.area == a && nodo.tipo == Punto.consolidamento && (consolidamento == null || carico_locale(consolidamento) > carico_locale(nodo)))
				consolidamento = nodo;
		return consolidamento;
	}
	public static void costi_magazzini() {
		for (Nodo nodo : nodi)
			switch(nodo.tipo) {
				case consolidamento:
					nodo.costi += costo_magazzino_consolidamento * nodo.magazzino;
					break;
				case gomma:
					nodo.costi += costo_magazzino_gomma * nodo.magazzino;
					break;
				case ferrovia:
					nodo.costi += costo_magazzino_ferroviario * nodo.magazzino;
					break;
				case origine:
					nodo.costi += (costo_magazzino_origine / 2) * nodo.magazzino;
					break;
			}
	}
	public static void formazione_treni() {
		for (Risorsa risorsa : risorse)
			if(risorsa instanceof Treno) {
				Treno treno = (Treno)risorsa;
				if (treno.in_partenza()) {
					treno.materiale_in_n = true;
					treno.materiale_in_m = true;
				}
			}
	}
	public static Costo costo_logistico_totale() {
		Costo costi = new Costo();
		for (Risorsa risorsa : risorse) {
			if (risorsa instanceof Camion) {
				costi.fissi_risorse += (risorsa.utilizzata > 0 ? 1 : 0) * (((Camion)risorsa).arco.tipo == Tratto.lungo ? costo_fisso_lunga_distanza : costo_fisso_media_distanza);
				costi.vuoti += ((Camion)risorsa).costi_vuoti;
			} else if (risorsa instanceof Treno) {
				costi.fissi_risorse += risorsa.utilizzata * costo_fisso_treno;
				costi.vuoti += ((Treno)risorsa).violazioni_capacita_minima * penalita_carico_minimo_treno;
			}
			costi.trasporto += risorsa.costi_trasporto;
		}
		for (Evento evento : eventi)
			if (evento instanceof Arrivo)
				for (Frammento frammento : evento.frammenti)
					if (evento.nodo == frammento.ordine.destinazione && (int)evento.tempo > frammento.ordine.duedate && (int)evento.tempo <= frammento.ordine.deadline)	
						costi.ritardi += costo_ritardo * frammento.qta * (int)(evento.tempo - frammento.ordine.duedate);
		for (Ordine ordine : ordini)
			if (ordine.scaduto)
				costi.ritardi += penalita_ordine_non_consegnato;
		for (Nodo nodo : nodi)
			costi.magazzino += nodo.costi;
		return costi;
	}
	public static void azzera() {
		for (Ordine ordine : ordini) {
			ordine.attivo = false;
			ordine.arrivato = 0;
			ordine.scaduto = false;
		}
		for (int e = 0; e < eventi.size(); e++) {
			eventi.get(e).attivo = true;
			if (eventi.get(e) instanceof Arrivo) {
				eventi.remove(e);
				e--;
			}
		}
		for (Nodo nodo : nodi) {
			nodo.costi = 0;
			nodo.magazzino = 0;						
		}
		for (Risorsa risorsa : risorse) {
			risorsa.costi_trasporto = 0;
			risorsa.utilizzata = 0;
			if (risorsa instanceof Camion) {
				((Camion)risorsa).nodo = ((Camion)risorsa).iniziale;
				((Camion)risorsa).costi_vuoti = 0;
			}
			if (risorsa instanceof Treno)
				((Treno)risorsa).violazioni_capacita_minima = 0;
		}
	}
	public static Percorso aggiungi_percorso(Percorso p) {
		for (Percorso percorso : percorsi)
			if (percorso.partenza == p.partenza && percorso.arrivo == p.arrivo)
				return percorso;
		percorsi.add(p);
		return p;
	}
	public static void stampa_piano() {
		FileWriter fw = null;
		PrintWriter pw = null;
		try {
			fw = new FileWriter(Base.percorso_uscita_piano);
			pw = new PrintWriter(fw);
			pw.println("Istanza;Ordine;Nodo partenza;Giorno partenza;Orario partenza;Nodo arrivo;Giorno arrivo;Orario arrivo;Risorsa;Contenitori");
			for (Ordine ordine : Rete.ordini) {
				for (Evento evento : Rete.eventi)
					if (evento instanceof Partenza) {
						Partenza partenza = (Partenza)evento;
						for (Frammento frammento : partenza.frammenti)
							if (frammento.ordine == ordine && partenza.arrivo != null) {
								pw.println(Base.istanza + ";" + ordine.id + ";" + partenza.nodo.id + ";" + partenza.tempo + ";;" + + partenza.arrivo.nodo.id + ";" + partenza.arrivo.tempo + ";;" + partenza.risorsa.id + ";" + frammento.qta);
								//pw.println(Base.istanza + ";" + ordine.id + ";" + partenza.nodo.id + ";" + Orologio.tempo_in_giorni(partenza.tempo) + ";" + Orologio.tempo_in_orario(partenza.tempo) + ";" + + partenza.arrivo.nodo.id + ";" + Orologio.tempo_in_giorni(partenza.arrivo.tempo) + ";" + Orologio.tempo_in_orario(partenza.arrivo.tempo) + ";" + partenza.risorsa.id + ";" + frammento.qta);
								//System.out.println(frammento.qta + "u da " + partenza.nodo + " il " + partenza.tempo + " a " + partenza.arrivo.nodo + " il " + partenza.arrivo.tempo + " con " + partenza.risorsa);
							}
					}
			}
			fw.close();
		} catch (Exception e) {
			System.err.println("errore nella scrittura del piano");
		}
	}
	public static void stampa_percorsi() {
		for (Nodo nodo : Rete.nodi)
			if(nodo.percorsi != null) {
				System.out.println(nodo);
				for (Percorso percorso : nodo.percorsi)
					System.out.println("\t" + percorso);
			}
	}
	public static void stampa() {
		for (Nodo nodo : nodi)
			System.out.println(nodo);
		System.out.println("velocità camion: " + velocita_camion);
		System.out.println("coefficiente velocità treno: " + coefficiente_treno_camion);
		for (Arco arco : archi)
			System.out.println(arco + ": " + arco.lunghezza + "km");
		for (Ordine ordine : ordini)
			System.out.println(ordine);
		System.out.println("capacità locale: " + capacita_max_locale);
		System.out.println("capacità camion: " + capacita_max_camion);
		System.out.println("capacità treno: " + capacita_max_treno);
		for (Risorsa risorsa : risorse)
			System.out.println(risorsa + (risorsa instanceof Camion ? ": " + ((Camion)risorsa).arco + ", " + ((Camion)risorsa).nodo : ""));
		System.out.println("costo fisso lunga distanza: " + costo_fisso_lunga_distanza);
		System.out.println("costo fisso media distanza: " + costo_fisso_media_distanza);
		System.out.println("costo fisso treno: " + costo_fisso_treno);
		System.out.println("costo magazzino origine: " + costo_magazzino_origine);
		System.out.println("costo magazzino consolidamento: " + costo_magazzino_consolidamento);
		System.out.println("costo magazzino gomma: " + costo_magazzino_gomma);
		System.out.println("costo magazzino ferrovia: " + costo_magazzino_ferroviario);		
		System.out.println("costo lunga distanza: " + costo_lunga_distanza);
		System.out.println("costo media distanza: " + costo_media_distanza);
		System.out.println("costo breve distanza: " + costo_breve_distanza);
		System.out.println("costo treno: " + costo_ferrovia);
		System.out.println("costo ritardo: " + costo_ritardo);
		for (Percorso percorso : percorsi) {
			System.out.println("da " + percorso.partenza + " a " + percorso.arrivo + " con " + percorso.tipo + ": " + percorso.capacita + "u");
			for (Arco arco : percorso.archi)
					System.out.println("\t" + arco);
		}
	}
}
