
public class Arrivo extends Evento {

	public Arrivo(Partenza partenza) {
		frammenti = partenza.frammenti;
		arco = partenza.arco;
		risorsa = partenza.risorsa;
		nodo = arco.altro_capo(partenza.nodo);
		arco.realizzazione_viaggio(partenza, this);
	}
	public void arriva() {
		risorsa.scarica(this);
		for (Frammento frammento : frammenti) {
			if (frammento.nodo != frammento.ordine.destinazione) {
				nodo.magazzino += frammento.qta;
				if (frammento.ordine.attivo) {
					frammento.ordine.deframmenta(frammento.nodo);
					frammento.ordine.schedula_partenze(frammento.nodo);
				}
			} else {
				frammento.ordine.arrivato += frammento.qta;
				if (frammento.ordine.arrivato())
					System.out.println(frammento.ordine + " arrivato");
			}
		}
		attivo = false;
	}
	public String toString() {
		return "arrivo di " + carico() + "u " + (frammenti.size() == 1 ? ("di " + frammenti.get(0).ordine) : "") + " da " + nodo + " a " + arco.altro_capo(nodo) + " con " + risorsa + " il " + tempo;
	}
}		
