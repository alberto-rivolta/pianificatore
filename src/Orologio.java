

public class Orologio {

	public static int giorno;
	public static Orario orario;
	public static double tempo;
	public static int orizzonte;
	public static double soglia_mattino_pomeriggio;
	public static int ore_lavorative;
	public static double epsilon = 1e-10; // 1e-15 è troppo piccolo

	public static void avvia() {
		giorno = 0;
		orario = Orario.mattina;
		tempo = 0;
	}

	public static boolean incrementa() {
		if (orario == Orario.pomeriggio) {
			giorno++;
			orario = Orario.mattina;
			tempo = giorno;
			for (Ordine ordine : Rete.ordini)
				if (!ordine.arrivato() && tempo > ordine.deadline)
					ordine.scaduto = true;
			if (giorno > orizzonte)
				return false;
		} else {
			orario = Orario.pomeriggio;
			tempo = giorno + soglia_mattino_pomeriggio + epsilon;
		}
		return true;
	}
	public static boolean adesso(double tempo) {
		int g = (int)Math.floor(tempo);
		double o = tempo - g;
		return g == giorno && ((orario == Orario.mattina && o <= soglia_mattino_pomeriggio) || (orario == Orario.pomeriggio && o > soglia_mattino_pomeriggio));
	}
	public static int tempo_in_giorni(double tempo) {
		return (int)Math.floor(tempo);
	}
	public static Orario tempo_in_orario(double tempo) {
		if (tempo - Math.floor(tempo) > soglia_mattino_pomeriggio)
			return Orario.pomeriggio;
		else
			return Orario.mattina;
	}
	public static double giorno_orario_in_tempo(int g, Orario o) {
		return g + (o == Orario.mattina ? 0 : (soglia_mattino_pomeriggio + epsilon));
	}
	public static double prossima_mezza_giornata() {
		return (int)tempo + ((tempo - (int)tempo) >= soglia_mattino_pomeriggio ? 1 : soglia_mattino_pomeriggio);
	}
}
