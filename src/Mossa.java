import java.util.ArrayList;


public abstract class Mossa {
	
	protected int i, j;
	protected double ampiezza = 1;
	protected ArrayList<Ordine> ordini;

	protected void aree() {	
		Coppia coppia = Euristica.soluzione.get(Rete.generatore.nextInt(Euristica.soluzione.size()));
		i = coppia.i;
		j = coppia.j;
		ordini = Rete.generatore.nextDouble() < 0.5 ? coppia.avanti : coppia.indietro;
	}
	protected int indice_vincolato(int n) {
		int estremo;
		if (n == 0)
			estremo = ordini.size() - 1;
		else if (n == ordini.size() - 1)
			estremo = 0;
		else
			estremo = Rete.generatore.nextDouble() < 0.5 ? 0 : ordini.size() - 1;
		int distanza = Math.min(Math.abs(n - estremo), (int)Math.floor(ampiezza * ordini.size()));
		return distanza > 0 ? n + (estremo == 0 ? -1 : 1) * (1 + Rete.generatore.nextInt(distanza)) : 0;
	}
	public void muovi() {
		aree();
	}
	public void annulla() {
		
	}
}
