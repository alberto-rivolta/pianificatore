import java.util.Collections;


public class Scambio extends Mossa {

	private int n;
	private int m;
	
	public Scambio() {
		
	}
	public Scambio(double a) {
		ampiezza = a;
	}
	public void muovi() {
		super.muovi();
		n = Rete.generatore.nextInt(ordini.size());
		m = indice_vincolato(n);
		Collections.swap(ordini, n, m);
	}
	public void annulla() {
		super.annulla();
		Collections.swap(ordini, n, m);
	}
	public String toString() {
		return "scambio (" + (ampiezza < 1 ? (ampiezza * 100) + "%" : "libero") + "): (" + i + " -> " + j + "), " + n + " con " + m;
	}
}
