import java.util.ArrayList;


public class Coppia {

	public int i;
	public int j;
	public ArrayList<Ordine> avanti = new ArrayList<>();
	public ArrayList<Ordine> indietro = new ArrayList<>();
	
	public Coppia(int i, int j) {
		this.i = i;
		this.j = j;
	}
}
