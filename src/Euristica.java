import java.io.FileWriter;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;


public abstract class Euristica {

	public static final int CASUALE = 0;
	public static final int SINGOLA = 1;
	public static final int VND = 2;
	public static final int RISCALDAMENTO = 3;
	public static ArrayList<Coppia> soluzione = new ArrayList<>();
	public static ArrayList<Mossa> mosse = new ArrayList<>();
	public static double peggioramento_iniziale_accettato;
	public static double ricettivita_peggioramento_iniziale;
	public static double coefficiente_raffreddamento;
	public static double temperatura_congelamento;
	public static int iterazioni_per_raffreddamento;
	protected static double t;
	protected static Costo z, e, migliore, iniziale;
	protected static int iterazione = 0;
	protected static double miglioramento;
	protected static Mossa mossa;
	protected static int iterazioni_senza_variazioni;
	
	public void esegui() {
		soluzione_iniziale();
		
		iniziale = valuta();
		Rete.stampa_piano();
		z = iniziale;
		t = temperatura_iniziale();
		e = z;
		migliore = z;
				
		Rete.generatore = new Random();

		System.out.println("costo iniziale: " + iniziale.totale());
		
		FileWriter fw = null;
		PrintWriter pw = null;
		try {
			fw = new FileWriter(Base.percorso_uscita_euristica);
			pw = new PrintWriter(fw);
			
			System.out.println("\ttemperatura\tcosto");
			pw.println("Iterazione;Accettato;Trasporto;Magazzino;Risorse;Vuoti;Ritardi;Migliore");
			pw.println(iterazione + ";" + z.totale() + ";" + z.trasporto + ";" + z.magazzino + ";" + z.fissi_risorse + ";" + z.vuoti + ";" + z.ritardi + ";" + migliore.totale());
			while (t > temperatura_congelamento) {
				mossa = prossima_mossa();
				mossa.muovi();
				e = valuta();
				miglioramento = z.totale() - e.totale();
				if (e.totale() < z.totale() || Rete.generatore.nextDouble() < Math.exp(-((e.totale() - z.totale()) / t))) {
					iterazioni_senza_variazioni = 0;
					z = e;
					if (e.totale() < migliore.totale()) {
						migliore = e;
						Rete.stampa_piano();
					}
				} else {
					iterazioni_senza_variazioni++;
					mossa.annulla();
				}
				if (iterazione % iterazioni_per_raffreddamento == 0)
					aggiorna_temperatura();
				iterazione++;
				pw.println(iterazione + ";" + z.totale() + ";" + z.trasporto + ";" + z.magazzino + ";" + z.fissi_risorse + ";" + z.vuoti + ";" + z.ritardi + ";" + migliore.totale());
				System.out.println(iterazione + "\t" + String.format("%.2e", t) + "\t" + z.totale());
			}
			System.out.println("costo iniziale: " + iniziale.totale());
			System.out.println("costo finale: " + migliore.totale());
			
			fw.close();
		} catch (Exception eccezione) {
			eccezione.printStackTrace();
			System.err.println("errore all'iterazione " + iterazione);
		}
		
	}
	protected void aggiorna_temperatura() {
		t *= coefficiente_raffreddamento;
	}
	protected Mossa prossima_mossa() {
		return mosse.get(Rete.generatore.nextInt(mosse.size()));
	}
	private static void soluzione_iniziale() {
		for (int i = 0; i < Rete.numero_aree - 1; i++) {
			for (int j = i + 1; j < Rete.numero_aree; j++) {
				Coppia coppia = new Coppia(i, j);
				for (Ordine ordine : Rete.ordini) {
					if (ordine.origine.area == i && ordine.destinazione.area == j) {
						coppia.avanti.add(ordine);
					} else if (ordine.origine.area == j && ordine.destinazione.area == i) {
						coppia.indietro.add(ordine);
					}
				}
				Collections.sort(coppia.avanti);
				Collections.sort(coppia.indietro);
				soluzione.add(coppia);
			}
		}
	}
	protected static double temperatura_iniziale() {
		return -((peggioramento_iniziale_accettato * iniziale.totale()) / Math.log(ricettivita_peggioramento_iniziale));
	}
	private static Costo valuta() {
		PrintStream uscita_originale = System.out;
		System.setOut(new PrintStream(new OutputStream() {
            public void write(int b) {

            }
        }));
		
		Rete.azzera();
		Rete.eventi.clear();
		for (Ordine ordine : Rete.ordini) {
			ordine.attivo = true;
			ordine.frammenti.clear();
		}
		for (Percorso percorso : Rete.percorsi)
			for (int i = 0; i < percorso.traffico.length; i++)
				percorso.traffico[i] = 0;		
		Rete.ordini.clear();
		ArrayList<Coppia> pianificate = new ArrayList<>();
		
		while(Euristica.soluzione.size() > 0) {
			Coppia coppia = Euristica.soluzione.remove(Rete.generatore.nextInt(Euristica.soluzione.size()));
			pianificate.add(coppia);
			int p = 0;
			while (p < coppia.avanti.size() || p < coppia.indietro.size()) {
				Rete.azzera();
				if (p < coppia.avanti.size())
					Rete.ordini.add(coppia.avanti.get(p));
				if (p < coppia.indietro.size())
					Rete.ordini.add(coppia.indietro.get(p));
				System.out.println();
				for (Ordine ordine : Rete.ordini)
					System.out.print(ordine + " ");
				System.out.println();
				
				Orologio.avvia();
				do {
					Rete.rilasci();
					Rete.formazione_treni();
					Evento evento;
					while ((evento = Rete.prossimo_evento()) != null) {
						Orologio.tempo = evento.tempo;
						System.out.println("\n" + Orologio.tempo + " (giorno " + Orologio.giorno + ", " + Orologio.orario + ")\n");
						if (evento instanceof Arrivo)
							((Arrivo)evento).arriva();
						else
							((Partenza)evento).tenta_partenza();
					}
					Rete.costi_magazzini();
				} while (Orologio.incrementa());				
							
				p++;
			}
		}
					
		for (Coppia coppia : pianificate)
			Euristica.soluzione.add(coppia);		
		System.setOut(uscita_originale);		
		return Rete.costo_logistico_totale();
	}
}
