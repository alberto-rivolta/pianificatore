
public class Singola extends Euristica {

	public Singola(Mossa m) {
		mossa = m;
	}
	public Mossa prossima_mossa() {
		return mossa;
	}
	public String toString() {
		return "simulated annealing con mossa fissa";
	}
}
