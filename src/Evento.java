import java.util.ArrayList;


public abstract class Evento implements Comparable<Evento> {

	public double tempo;
	public Arco arco;
	public Nodo nodo;
	public Risorsa risorsa;
	public ArrayList<Frammento> frammenti;
	public boolean attivo = true;

	public boolean imminente() {
		return attivo && Orologio.adesso(tempo);
	}
	public boolean vuoto() {
		return frammenti.size() == 0;
	}
	public int carico() {
		int c = 0;
		for (Frammento frammento : frammenti)
			c += frammento.qta;
		return c;
	}
	public int compareTo(Evento e) {
		int confronto = Double.compare(tempo, e.tempo);
		if (confronto == 0) {
			if (e instanceof Arrivo && this instanceof Partenza)
				return 1;
			else if (e instanceof Partenza && this instanceof Arrivo)
				return -1;
		}
		return confronto;
	}
}
