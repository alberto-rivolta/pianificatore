
public class Costo {

	public double trasporto = 0;
	public double magazzino = 0;
	public double fissi_risorse = 0;
	public double vuoti = 0;
	public double ritardi = 0;
	public double totale() {
		return trasporto + magazzino + fissi_risorse + vuoti + ritardi;
	}
}
