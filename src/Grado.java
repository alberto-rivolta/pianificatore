import java.util.ArrayList;


public class Grado implements Comparable<Grado>{

	public static final int UTILE = 0;
	public static final int VELOCE = 1;
	public Risorsa risorsa;
	public Nodo nodo;
	public double tempo = -1;
	public Partenza utile;
	Evento ultimo;
	public ArrayList<Partenza> ulteriori = new ArrayList<>();
	
	public Grado(Risorsa r, Nodo n, Arco a, int t) {
		nodo = n;
		risorsa = r;
		ArrayList<Evento> eventi = risorsa.eventi();
		if (t == UTILE) {
			for (Evento evento : eventi) {
				if (evento instanceof Partenza && evento.nodo == n && evento.carico() < risorsa.capacita_max()) {
					tempo = ((Partenza)evento).tempo;
					utile = (Partenza)evento;
					return;
				}
			}
		} else {
			if (risorsa instanceof Treno) {
				double t1 = ((Treno)risorsa).prossima_partenza();
				if (eventi.size() == 0) {
					tempo = t1;
				} else {
					double t2;
					for (Evento evento : eventi) {
						if (evento instanceof Partenza && evento.nodo == nodo) {
							t2 = evento.tempo;
							if (t2 > t1) {
								tempo = t1;
								return;
							}
							if (evento.carico() < risorsa.capacita_max()) {
								tempo = t2;
								return;
							}
							t1 = t2 + ((Treno)risorsa).frequenza;
						}
					}
					tempo = t1;
				}
			} else {
				if (eventi.size() == 0) {
					tempo = Orologio.tempo + Orologio.epsilon;
					if (((Camion)risorsa).nodo != nodo) {
						ulteriori.add(new Partenza(Orologio.tempo, a, a.altro_capo(nodo), risorsa));
						tempo += a.durata(risorsa) + Orologio.epsilon;
					}
					return;
				}
				ultimo = eventi.get(eventi.size() - 1);
				tempo = ultimo.tempo + Orologio.epsilon;
				if (ultimo instanceof Arrivo) {
					if (ultimo.nodo != nodo) {
						tempo += a.durata(risorsa) + Orologio.epsilon;
						ulteriori.add(new Partenza(ultimo.tempo + Orologio.epsilon, a, a.altro_capo(nodo), risorsa));
					}
				} else {
					if (ultimo.nodo == nodo) {
						tempo += a.durata(risorsa) + Orologio.epsilon;
						ulteriori.add(new Partenza(ultimo.tempo + a.durata(risorsa) + Orologio.epsilon, a, a.altro_capo(nodo), risorsa));
					}
					tempo += a.durata(risorsa);
				}
			}
		}
	}
	public String toString() {
		return risorsa + ": " + tempo;
	}
	public int compareTo(Grado g) {
		return Double.compare(tempo, g.tempo);
	}
}
