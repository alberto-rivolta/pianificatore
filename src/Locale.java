
public class Locale extends Risorsa {

	public Locale(int id_r) {
		id = id_r;
	}
	public int capacita_max() {
		return Rete.capacita_max_locale;
	}
	public double velocita() {
		return Rete.velocita_locale;
	}
	public boolean disponibile(Partenza partenza) {
		Nodo consolidamento = partenza.arco.n.tipo == Punto.consolidamento ? partenza.arco.n : partenza.arco.m;
		return partenza.frammenti.get(0).ordine.attivo ? Rete.carico_locale(consolidamento) <= capacita_max() : true;
	}
	public String toString() {
		return "flotta locale";
	}
}
