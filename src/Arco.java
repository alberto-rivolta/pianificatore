import java.util.ArrayList;


public class Arco {

	public Nodo n;
	public Nodo m;
	public double lunghezza;
	public Tratto tipo;

	public Arco(int id_n, int id_m, double l, String t) {
		n = Rete.nodo(id_n);
		m = Rete.nodo(id_m);
		n.adiacenti.add(m);
		m.adiacenti.add(n);
		lunghezza = l;
		if (t.equals("Rail"))
			tipo = Tratto.ferrovia;
		else if (t.equals("RoadLocal"))
			tipo = Tratto.locale;
		else if (t.equals("RoadMid"))
			tipo = Tratto.medio;
		else if (t.equals("RoadLong"))
			tipo = Tratto.lungo;
		}
	public Nodo altro_capo(Nodo capo) {
		if (n == capo)
			return m;
		else if (m == capo)
			return n;
		else
			return null;
	}
	public double durata(Risorsa risorsa) {
		return (lunghezza / risorsa.velocita()) / Orologio.ore_lavorative;
	}
	public void realizzazione_viaggio(Partenza partenza, Arrivo arrivo) {
		double durata = durata(partenza.risorsa);
		double ritardo = Rete.generatore.nextDouble() * durata * Rete.ritardo_massimo;
		arrivo.tempo = partenza.tempo + durata + ritardo;
		System.out.println("aggiunti " + ritardo + " giorni di ritardo al viaggio");
	}
	public ArrayList<Risorsa> risorse() {
		ArrayList<Risorsa> risorse = new ArrayList<>();
		for (Risorsa risorsa : Rete.risorse)
			if (risorsa.arco == this)
				risorse.add(risorsa);
		return risorse;
	}
	public String toString() {
		return "arco " + n.id + "-" + m.id + " (" + tipo + ")";
	}
}
