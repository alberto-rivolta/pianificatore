
public class Camion extends Risorsa{

	public Nodo nodo;
	public double costi_vuoti = 0;
	public Nodo iniziale;

	public Camion(int id_r, Arco a, Nodo n) {
		id = id_r;
		arco = a;
		nodo = n;
		iniziale = n;
	}
	public int capacita_max() {
		return Rete.capacita_max_camion;
	}
	public double velocita() {
		return Rete.velocita_camion;
	}
	public boolean disponibile(Partenza partenza) {
		return arco == partenza.arco && nodo == partenza.nodo && partenza.carico() <= capacita_max();
	}
	public void scarica(Arrivo arrivo) {
		super.scarica(arrivo);
		nodo = arrivo.nodo;
		utilizzata++;
		System.out.println(this + " è ora in " + nodo);
	}
	public void carica(Partenza partenza) {
		super.carica(partenza);
		nodo = null;
		costi_vuoti += Rete.costo_trasporto_vuoto * (capacita_max() - partenza.carico()) * arco.lunghezza;
		System.out.println(this + " è ora in viaggio verso " + arco.altro_capo(partenza.nodo));
	}
	public String toString() {
		return "camion " + id;
	}
}
