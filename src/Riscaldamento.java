
public class Riscaldamento extends Euristica {

	private int iterazioni_per_riscaldamento;
	private int iterazioni_massime;
	
	public Riscaldamento(int i, int m) {
		iterazioni_per_riscaldamento = i;
		iterazioni_massime = m;
	}
	public void aggiorna_temperatura() {
		if (iterazioni_senza_variazioni == iterazioni_per_riscaldamento && iterazione < iterazioni_massime)
			t = temperatura_iniziale();
		else
			super.aggiorna_temperatura();
	}
	public String toString() {
		return "simulated annealing con riscaldamento";
	}
}
