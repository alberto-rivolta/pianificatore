import java.sql.*;


public class Base {

	public static String percorso_base;
	public static String percorso_uscita_piano;
	public static String percorso_uscita_euristica;
	public static int istanza;

	public static void leggi() throws SQLException {
		System.out.println("lettura base di dati...");
		Connection connessione = DriverManager.getConnection("jdbc:ucanaccess://" + percorso_base);
		ResultSet risultato;
	    
		risultato = connessione.createStatement().executeQuery("select * from DatiInst where inst = " + istanza);
		while (risultato.next()) {
			Orologio.orizzonte = risultato.getInt(risultato.findColumn("PlanningHorizon"));
			Rete.capacita_max_camion = risultato.getInt(risultato.findColumn("LongDistCap"));
			Rete.capacita_max_treno = risultato.getInt(risultato.findColumn("MaxTrainCap"));
			Rete.capacita_min_treno = risultato.getInt(risultato.findColumn("MinTrainCap"));
			Rete.capacita_max_locale = risultato.getInt(risultato.findColumn("ShortDistCap"));
			Rete.numero_aree = risultato.getInt(risultato.findColumn("numAree"));
			Rete.velocita_camion = risultato.getInt(risultato.findColumn("LongDistVelTruck"));
			Rete.velocita_locale = risultato.getInt(risultato.findColumn("ShortDistVelTruck"));
			Rete.coefficiente_treno_camion = risultato.getDouble(risultato.findColumn("TruckTrainSpeedFactor"));
			Rete.costo_lunga_distanza = risultato.getDouble(risultato.findColumn("CostoLongDistRoad"));
			Rete.costo_media_distanza = risultato.getDouble(risultato.findColumn("CostoMidDistRoad"));
			Rete.costo_breve_distanza = risultato.getDouble(risultato.findColumn("CostoShortDistRoad"));
			Rete.costo_ferrovia = risultato.getDouble(risultato.findColumn("CostoRail"));
			Rete.costo_magazzino_origine = risultato.getDouble(risultato.findColumn("CostoInvO"));
			Rete.costo_magazzino_consolidamento = risultato.getDouble(risultato.findColumn("CostoInvCD"));
			Rete.costo_magazzino_ferroviario = risultato.getDouble(risultato.findColumn("CostoInvFG"));
			Rete.costo_magazzino_gomma = risultato.getDouble(risultato.findColumn("CostoInvGG"));
			Rete.costo_ritardo = risultato.getDouble(risultato.findColumn("CostoTardiness"));
			Rete.costo_fisso_media_distanza = risultato.getDouble(risultato.findColumn("FixedCostRoadMid"));
			Rete.costo_fisso_lunga_distanza = risultato.getDouble(risultato.findColumn("FixedCostRoadLong"));
			Rete.costo_fisso_treno = risultato.getDouble(risultato.findColumn("FixedCostTrain"));
			Rete.costo_trasporto_vuoto = risultato.getDouble(risultato.findColumn("CostNotFullTrasp"));
		}
	    
		risultato = connessione.createStatement().executeQuery("select * from Nodes where Instance  = " + istanza);
		while (risultato.next())
			Rete.nodi.add(new Nodo(risultato.getInt(risultato.findColumn("IDnode")), risultato.getInt(risultato.findColumn("IDarea")),risultato.getString(risultato.findColumn("Type"))));
		risultato = connessione.createStatement().executeQuery("select * from Distances where Instance  = " + istanza);
		while (risultato.next())
			Rete.archi.add(new Arco(risultato.getInt(risultato.findColumn("Origin")), risultato.getInt(risultato.findColumn("Destination")), risultato.getDouble(risultato.findColumn("Distance")), risultato.getString(risultato.findColumn("ArcType"))));

		risultato = connessione.createStatement().executeQuery("select * from Orders where Instance  = " + istanza);
		while (risultato.next())
			Rete.ordini.add(new Ordine(risultato.getInt(risultato.findColumn("OrId")), risultato.getInt(risultato.findColumn("Quant")), risultato.getInt(risultato.findColumn("Release")), risultato.getInt(risultato.findColumn("Orig")), risultato.getInt(risultato.findColumn("Dest")), risultato.getInt(risultato.findColumn("Duedate")), risultato.getInt(risultato.findColumn("Deadline"))));
        
		risultato = connessione.createStatement().executeQuery("select * from ArcResPos where instance  = " + istanza);
		while (risultato.next()) {
			Arco arco = Rete.arco(Rete.nodo(risultato.getInt(risultato.findColumn("nodoi"))), Rete.nodo(risultato.getInt(risultato.findColumn("nodoj"))));
			if (risultato.getInt(risultato.findColumn("APdepTrain")) == -1) {
				Camion camion = new Camion(risultato.getInt(risultato.findColumn("IdRes")), arco, Rete.nodo(risultato.getInt(risultato.findColumn("Pos"))));
				Rete.risorse.add(camion);
			} else {
				Treno treno = new Treno(risultato.getInt(risultato.findColumn("IdRes")), arco, risultato.getInt(risultato.findColumn("FreqTrain")), risultato.getInt(risultato.findColumn("APdepTrain")));
				Rete.risorse.add(treno);
			}
		}
		
		connessione.close();			
		System.out.println("base di dati letta");
    }
}
