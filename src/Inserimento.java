
public class Inserimento extends Mossa {

	private int k;
	private int l;
	
	public Inserimento() {
		
	}
	public Inserimento(double a) {
		ampiezza = a;
	}
	public void muovi() {
		super.muovi();
		k = Rete.generatore.nextInt(ordini.size());
		l = indice_vincolato(k);
		ordini.add(l, ordini.remove(k));
	}
	public void annulla() {
		super.annulla();
		ordini.add(k, ordini.remove(l));
	}
	public String toString() {
		return "inserimento (" + (ampiezza < 1 ? (ampiezza * 100) + "%" : "libero") + "): (" + i + " -> " + j + "), da " + k + " a " + l;
	}
}
