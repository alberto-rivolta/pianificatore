import java.util.ArrayList;
import java.util.Collections;


public class Nodo implements Comparable<Nodo> {
	
	public int id;
	public int area;
	public Punto tipo;
	public int magazzino = 0;
	public double costi = 0;
	ArrayList<Nodo> adiacenti = new ArrayList<>();
	ArrayList<Percorso> percorsi;

	public Nodo(int n, int a, String t) {
		id = n;
		area = a;
		if (t.equals("OD"))
			tipo = Punto.origine;
		else if (t.equals("CD")) {
			tipo = Punto.consolidamento;
			percorsi = new ArrayList<>();
		}
		else if (t.equals("FG"))
			tipo = Punto.ferrovia;
		else if (t.equals("GG"))
			tipo = Punto.gomma;
	}
	public ArrayList<Percorso> percorsi_verso(int a) {
		ArrayList<Percorso> lista = new ArrayList<>();
		for (Percorso percorso : percorsi)
			if (percorso.arrivo.area == a)
				lista.add(percorso);
		Collections.sort(lista);
		return lista;
	}
	public int compareTo(Nodo n) {
		return Integer.compare(id, n.id);
	}
	public String toString() {
		return "nodo " + id + " (area " + area + ", " + tipo + ")";
	}
}
