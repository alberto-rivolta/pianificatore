import java.util.ArrayList;


public class Graduatoria {
	private int i = 0;
	private int j = 0;
	public ArrayList<Grado> voci = new ArrayList<>();
	
	public Graduatoria(Nodo nodo, Arco arco, ArrayList<Risorsa> risorse) {
		for (Risorsa risorsa : risorse) {
			Grado utile = new Grado(risorsa, nodo, arco, Grado.UTILE);
			Grado veloce = new Grado(risorsa, nodo, arco, Grado.VELOCE);
			if (utile.tempo > 0) {
				inserisci(utile);
				if (veloce.tempo != utile.tempo) {
					inserisci(veloce);
				}
			} else {
				inserisci(veloce);
			}
		}
	}
	private void inserisci(Grado grado) {
		if (grado.risorsa instanceof Treno) {
			if (grado.utile != null) {
				if (grado.utile.carico() <= ((Treno)grado.risorsa).capacita_min()) {
					voci.add(0, grado);
					i++; j++;
				} else {
					voci.add(i, grado);
					j++;
				}
			} else {
				int k = j;
				while (k < voci.size()) {
					if (voci.get(k).tempo > grado.tempo)
						break;
					k++;
				}
				voci.add(k, grado);
			}
		} else {
			if (grado.utile != null) {
				voci.add(0, grado);
				i++; j++;
			} else if (((Camion)grado.risorsa).nodo == grado.nodo && grado.ultimo == null) {
				int k = i;
				if (grado.risorsa.utilizzata == 0) {
					while (k < j) {
						if (voci.get(k).risorsa.utilizzata == 0)
							break;
						k++;
					}
				}
				voci.add(k, grado);						
				j++;
			} else {
				int k = j;
				while (k < voci.size()) {
					if (voci.get(k).tempo > grado.tempo || (voci.get(k).tempo == grado.tempo && grado.risorsa.utilizzata > 0 && voci.get(k).risorsa.utilizzata == 0))
						break;
					k++;
				}
				voci.add(k, grado);
			}
		}
	}
}
