import java.util.ArrayList;


public class Partenza extends Evento {

	public Arrivo arrivo;
	public Partenza(double t, Arco a, Nodo p, Risorsa r) {
		tempo = t;
		arco = a;
		nodo = p;
		risorsa = r;
		frammenti = new ArrayList<Frammento>();
	}
	public Partenza(Frammento f, double t, Arco a, Nodo p, Risorsa r) {
		this(t, a, p, r);
		frammenti.add(f);
	}
	public void tenta_partenza() {
		if (risorsa.disponibile(this))
			parti();
		else
			ripianifica();
	}
	public void parti() {
		risorsa.carica(this);
		arrivo = new Arrivo(this);
		attivo = false;
		Rete.eventi.add(arrivo);
		System.out.println("arrivo pianificato in " + arrivo.tempo);
	}
	public void frammentazione_locale(int invio, Frammento frammento) {
		if (invio > 0) {
			Frammento f = new Frammento(frammento.ordine, invio, nodo);
			frammento.ordine.frammenti.add(f);
			frammento.qta -= invio;
			Partenza p = new Partenza(f, tempo, arco, nodo, risorsa);
			Rete.eventi.add(p);
			System.out.println("aggiunta " + p);
		}
	}
	public void ripianifica() {
		if (arco.tipo == Tratto.locale) {
			System.out.println(this + " non è possibile");
			System.out.println("superata capacità flotta locale");
			Frammento frammento = frammenti.get(0);
			int capacita_locale_residua = Rete.capacita_max_locale - (Rete.carico_locale(nodo.tipo == Punto.consolidamento ? nodo : arco.altro_capo(nodo)) - carico());
			if (nodo.tipo == Punto.consolidamento) {
				if (capacita_locale_residua > 0) {
					Frammento f = new Frammento(frammento.ordine, capacita_locale_residua, nodo);
					frammento.ordine.frammenti.add(f);
					frammento.qta -= capacita_locale_residua;
					Partenza p = new Partenza(f, tempo, arco, nodo, risorsa);
					Rete.eventi.add(p);
					System.out.println("aggiunta " + p);
				}
				tempo = (int)tempo + 1;
				System.out.println("il resto del carico verrà spedito il " + tempo);
			} else {
				int contenitore = Rete.capacita_max_camion; // FIXME dimensione container
				Nodo consolidamento = arco.altro_capo(nodo);
				ArrayList<Percorso> percorsi = consolidamento.percorsi_verso(frammento.ordine.destinazione.area);
				for (Percorso percorso : percorsi) {
					int capacita_residua_percorso = (int)percorso.capacita - percorso.traffico[Orologio.giorno];
					if (capacita_residua_percorso > 1) {
						int invio = Math.min(capacita_locale_residua, contenitore - percorso.traffico[Orologio.giorno] % contenitore);
						frammentazione_locale(invio, frammento);
						capacita_locale_residua -= invio;
						capacita_residua_percorso -= invio;
						invio = (int)Math.min(Math.floor(capacita_locale_residua / contenitore), Math.floor(capacita_residua_percorso / contenitore));
						frammentazione_locale(invio, frammento);
						capacita_locale_residua -= invio;
						capacita_residua_percorso -= invio;
						break;
					}
				}
				tempo = (int)tempo + 1;
				System.out.println("il resto del carico verrà spedito il " + tempo);
			}
		} else {
			System.err.println(this + " non è possibile");
			System.out.println("la risorsa " + risorsa + " risulta non disponibile");
			System.out.println("eventi schedulati:");
			ArrayList<Evento> ei = risorsa.eventi();
			for (Evento e : ei)
				System.out.println("\t" + e);
			if (risorsa instanceof Treno) {
				ArrayList<Evento> eventi = risorsa.eventi();
				Partenza ultima_partenza = this;
				boolean ripianificata = false;
				for (Evento evento : eventi) {
					if (evento.tempo > Orologio.tempo && evento instanceof Partenza && evento.nodo == nodo) {
						ultima_partenza = (Partenza)evento;
						if (risorsa.capacita_max() - evento.carico() >= carico()) {
							for (Frammento frammento : frammenti)
								evento.frammenti.add(frammento);
							System.out.println("partenza raggruppata a " + evento);
							Rete.eventi.remove(this);
							ripianificata = true;
							break;
						}
					}
				}
				if (!ripianificata)
						tempo = ultima_partenza != this ? (ultima_partenza.tempo + ((Treno)risorsa).frequenza) : ((Treno)risorsa).prossima_partenza();
			} else {
				boolean in_arrivo = false;
				for (Evento evento : Rete.eventi) {
					if (evento instanceof Arrivo && evento.tempo >= Orologio.tempo && evento.risorsa == risorsa && evento.nodo == nodo) {
						in_arrivo = true;
						tempo = evento.tempo + Orologio.epsilon;
						System.out.println("partenza ripianificata in " + tempo + " per arrivo risorsa");
						break;
					}
				}
				if (!in_arrivo) {
					tempo = Orologio.prossima_mezza_giornata();
					System.out.println("partenza rimandata di mezza giornata in " + tempo + " per attesa di sviluppi");
				}
			}
		}
	}
	public String toString() {
		return "partenza di " + carico() + "u " + (frammenti.size() == 1 ? ("di " + frammenti.get(0).ordine) : "") + " da " + nodo + " a " + arco.altro_capo(nodo) + " con " + risorsa + " il " + tempo;
	}
}
